<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class ArticleController
 * @package App\Controller
 *
 * @Route("/article")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/{id}")
     * @param Article $article
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(
        Request $request,
        EntityManagerInterface $em,
        Article $article)
    {
        /*
         * Sous l'article, si l'utilisateur n'est pas connecté
         * l'inviter à la faire pour pouvoir poster un commentaire.
         * Sinon, lui afficher un formulaire avec un textarea pour pouvoir
         * écrire un commentaire.
         * Nécessite une entité Comment avec :
         * - content (text en bdd)
         * - publicationDate (datetime)
         * - user (l'utilisateur qui écrit le commentaire)
         * - article (l'article sur lequel le commentaire est écrit)
         * Nécessite aussi le form type avec le textarea du commentair
         * Validation : non vide
         *
         * Lister les commentaires en-dessous avec nom utilisateur, date et
         * contenu par ordre antéchronologique
         *
         *
         */
        $comment = new Comment();

        $form = $this->createForm(CommentType::class, $comment);

        $form->handleRequest($request);

        if ($form->isSubmitted()){
            if($form->isValid()){
                $comment
                    ->setUser($this->getUser())
                    ->setArticle($article);

                $em->persist($comment);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Votre commentaire est enregistré'
                );
                return $this->redirectToRoute(
                    // route de la page courante
                    $request->get('_route'),
                    [
                        'id' => $article->getId()
                    ]
                );
            }else{
                $this->addFlash(
                    'error',
                    'Le formulaire contient des erreurs.'
                );
            }
        }
        return $this->render('article/index.html.twig',
            [
            'article' => $article,
            'form' => $form->createView()
        ]
        );
    }
}
