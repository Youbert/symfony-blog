<?php


namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CategoryController
 * @package App\Controller\Admin
 *
 * préfixe de route pour toutes les routes définies dans cette classe
 * @Route("/categorie")
 */
class CategoryController extends AbstractController
{
    /**
     *
     * @Route("/")
     */
    public function index(CategoryRepository $categoryRepository)
    {

        //sans injecter le repository
        //$repo = $this->>getDoctrine()->getRepository(CategoryRepository::class)
        //ou
        //$em = $this->getDoctrine()->getManager();
        //$repo = $em->getRepository(CategoryRepository::class);

        //$category = $categoryRepository->findAll();
        //finAll() avec un tri sur le nom:
        $category = $categoryRepository->findBy([], ['name'=>'ASC']);
        return $this->render(
          'admin/category/index.html.twig',[
               'category'=>$category
            ]
        );
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * la partie variable id dans la route est optionnelle est vaut
     * null par défaut; si elle est présente, ce doit être un nombre
     * @Route("/edition/{id}",
     * defaults={"id": null},
     * requirements={"id": "\d+"})
     */
    public function edit(Request $request, EntityManagerInterface $em, $id
    ){
        if (is_null($id)){//création
            $category = new Category();
        } else {//modification
            $category = $em->find(Category::class, $id);

            //404 si l'id n'est pas en BDD
            if (is_null($category)){
                throw new NotFoundHttpException();
            }
        }

       // création du formulaire relié à la catégorie
        $form = $this->createForm(CategoryType::class, $category);

        // le formulaire analyse la requête et fait le mapping
        // avec l'entité s'il a été soumis
        $form->handleRequest($request);

        dump($category);

        // si le formulaire est envoyé / a été soumis
        if($form->isSubmitted()){
            if($form->isValid()){

                //enregistrement en bdd
                $em->persist($category);
                $em->flush();

                $this->addFlash('success', 'La catégorie est enregistrée.');

                //redirection vers la liste
                return $this->redirectToRoute('app_admin_category_index');
            }else{
                $this->addFlash('error', 'Le formulaire contient des erreurs');
            }
        }

        return $this->render('admin/category/edit.html.twig',
            [
                // passage du formulaire au template
                'form' => $form->createView()
            ]
        );
    }

    /**
     * ParamConverter : $category est un objet Category
     * correspondant à la catégorie dont on a reçu l'id dans l'URL
     * @Route("/suppression/{id}")
     */
    public function delete(
        EntityManagerInterface $em,
        Category $category
    ){
        $name = $category->getName();

        if (!$category->getArticles()->isEmpty()){
            $this->addFlash(
                'error',
                'La catégorie ne peut pas être supprimée car elle contient des articlesyes.'
            );
        }else{

            $em->remove($category);
            $em->flush();

            $this->addFlash('success', "La catégorie $name est supprimée");
        }
        return $this->redirectToRoute('app_admin_category_index');
    }
}